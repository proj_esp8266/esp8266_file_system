#include <osapi.h>
#include <user_interface.h>
#include <strings.h>
#include <stdlib.h>
#include <mem.h>

#include "user_uart.h"

#define SPI_FLASH_SIZE_MAP 4
#define SYSTEM_PARTITION_OTA_SIZE							0x6A000
#define SYSTEM_PARTITION_OTA_2_ADDR							0x81000
#define SYSTEM_PARTITION_RF_CAL_ADDR						0x3fb000
#define SYSTEM_PARTITION_PHY_DATA_ADDR						0x3fc000
#define SYSTEM_PARTITION_SYSTEM_PARAMETER_ADDR				0x3fd000

#define FILE_SYSTEM_ADDR    0xC0000
#define FILES_ADDR          0xC0100
#define FILES_BASE          0xC1000

#define EAGLE_FLASH_BIN_ADDR				（SYSTEM_PARTITION_CUSTOMER_BEGIN + 1）
#define EAGLE_IROM0TEXT_BIN_ADDR			（SYSTEM_PARTITION_CUSTOMER_BEGIN + 2）

static const partition_item_t partition_table[] = {
    { SYSTEM_PARTITION_CUSTOMER_BEGIN+1, 	0x00000, 0x1000},
    { SYSTEM_PARTITION_CUSTOMER_BEGIN+2, 0x10000, 0xA0000},
    { SYSTEM_PARTITION_RF_CAL, SYSTEM_PARTITION_RF_CAL_ADDR, 0x1000},
    { SYSTEM_PARTITION_PHY_DATA, SYSTEM_PARTITION_PHY_DATA_ADDR, 0x1000},
    { SYSTEM_PARTITION_SYSTEM_PARAMETER,SYSTEM_PARTITION_SYSTEM_PARAMETER_ADDR, 0x3000},
    { SYSTEM_PARTITION_CUSTOMER_BEGIN+3, FILE_SYSTEM_ADDR, 0x10000},
//    { SYSTEM_PARTITION_CUSTOMER_BEGIN+4, FILES_ADDR, 61440},
};

struct UartBuffer* set_uart_buffer(uint32 _size);

struct UartBuffer RxBuffer;
uint8 tmp_char;
uint8 command[1024];
uint16 pcommand = 0;
bool receiving_file = false;
uint8 *flash_buffer;
uint8 *flash_bkp;
uint16 size_buffer;
uint16 block_file;
uint16 size_bytes;
uint8 name[20];
uint16 size;

typedef struct{
    uint8 name[24];
    uint32 size;
    uint32 address;
}file_t;

struct{
    uint32 security_code_1;
    uint32 number_of_files;
    uint32 total_size;
    uint32 free_size;
    uint32 used_size;
    uint32 file;
    file_t *ptr;
    uint32 next_position;
    uint32 security_code_2;
}file_system;

typedef void (* states)(void *arg);

os_timer_t loop_timer;
#define LOOP_TIMER_TIME 100

uint32 ICACHE_FLASH_ATTR
user_rf_cal_sector_set(void)
{
    enum flash_size_map size_map = system_get_flash_size_map();
    uint32 rf_cal_sec = 0;
    switch (size_map)
    {
        case FLASH_SIZE_4M_MAP_256_256:
            rf_cal_sec = 128 - 5;
            break;
        case FLASH_SIZE_8M_MAP_512_512:
            rf_cal_sec = 256 - 5;
            break;
        case FLASH_SIZE_16M_MAP_512_512:
        case FLASH_SIZE_16M_MAP_1024_1024:
            rf_cal_sec = 512 - 5;
            break;
        case FLASH_SIZE_32M_MAP_512_512:
        case FLASH_SIZE_32M_MAP_1024_1024:
            rf_cal_sec = 512 - 5;
            break;
        case FLASH_SIZE_64M_MAP_1024_1024:
            rf_cal_sec = 2048 - 5;
            break;
        case FLASH_SIZE_128M_MAP_1024_1024:
            rf_cal_sec = 4096 - 5;
            break;
        default:
            rf_cal_sec = 0;
            break;
    }
    return rf_cal_sec;
}

void ICACHE_FLASH_ATTR
loop(void *arg)
{
    while (RxBuffer.Space < RxBuffer.UartBuffSize){
        tmp_char = *RxBuffer.pOutPos;
        if (!receiving_file){
            if (tmp_char != '\n'){
                if (tmp_char == '\r'){
                    command[pcommand++] = '\r';
                    command[pcommand] = 0x00;
                    pcommand = 0;
                    if (os_strstr(command, "ld\r") != NULL){
                        os_printf("File system:\n");
                        os_printf("Number of files: %u | ", file_system.number_of_files);
                        os_printf("Total size: %u kB | ", file_system.total_size/1024);
                        os_printf("Free size: %u kB | ", file_system.free_size/1024);
                        os_printf("Used size: %u\n", file_system.used_size/1024);
                        os_printf("\n\n");
                        os_printf("Name..........Size (bytes).........ADDR\n");
                        if (file_system.number_of_files > 0){
                            file_t _file;
                            for (int _i = 0; _i < file_system.number_of_files; _i++){
                                spi_flash_read(FILES_ADDR+_i*sizeof(file_t), &_file, sizeof(file_t));
                                os_printf("%s........%u........0x%08X\n", _file.name, _file.size, _file.address);
                            }
                        }
                        os_printf("------\n");
                    }
                    else if (os_strstr(command, "dw ") != NULL){
                        uint8* ptr = os_strchr(command, ' ');
                        uint8* ptr_1;
                        ptr++;
                        ptr_1 = os_strchr(ptr, ' ');
                        if(ptr_1 != NULL){
                            uint8 size_str[10];
                            uint8 _i;
                            os_strncpy(&name[0], ptr, ptr_1-ptr);
                            name[ptr_1-ptr] = 0;
                            ptr = ptr_1 + 1;
                            ptr_1 = os_strchr(ptr, '\r');
                            if ((ptr_1-ptr) > 0){
                                os_strncpy(&size_str[0], ptr, ptr_1-ptr);
                                size_str[ptr_1-ptr] = 0;
                                for (_i = 0; _i < (ptr_1-ptr); _i++){
                                    if ((size_str[_i] < '0') || (size_str[_i] > '9'))
                                        break;        
                                }
                                if (_i >= (ptr_1-ptr)){
                                    file_t _file;
                                    size = atoi(size_str);
                                    receiving_file = true;
                                    for (int _i = 0; _i < file_system.number_of_files; _i++){
                                        spi_flash_read(FILES_ADDR+_i*sizeof(file_t), &_file, sizeof(file_t));
                                        if (os_strcmp(name, _file.name) == 0){
                                            receiving_file = false;
                                            break;
                                        }
                                    }
                                    if (receiving_file){
                                        size_bytes = size/4096;
                                        if ((size%4096) > 0)
                                            size_bytes++;
                                        size_bytes *= 4096;
                                        if (size_bytes > 4096)
                                            flash_buffer = (uint8 *)os_malloc(4096);
                                        else    
                                            flash_buffer = (uint8 *)os_malloc(size_bytes);
                                        size_buffer = 0;
                                        block_file = 0;

                                        os_printf("Nome do arquivo: %s | Tamanho do arquivo: %u (%u)\n>\n", name, size, size_bytes);
                                    }
                                    else{
                                        os_printf("File already exist\n");
                                    }
                                }
                                else
                                    os_printf("Comando inválido\n");
                            }
                            else
                                os_printf("Comando inválido\n");
                        }
                        else
                            os_printf("Comando inválido\n");
                    }
                    else if (os_strstr(command, "erase\r") != NULL){
                        os_printf("Erasing file system\n");
                        file_system.number_of_files = 0;
                        file_system.total_size = 61440;
                        file_system.free_size = 61440;
                        file_system.used_size = 0;
                        file_system.ptr = FILES_ADDR;
                        file_system.next_position = FILES_BASE;

                        spi_flash_erase_sector(FILE_SYSTEM_ADDR/4096);
                        spi_flash_write(FILE_SYSTEM_ADDR, (uint32 *)&file_system, sizeof(file_system));
                    }
                    else if (os_strstr(command, "up ") != NULL){
                        uint8* ptr = os_strchr(command, ' ');
                        uint8* ptr_1;
                        ptr++;
                        ptr_1 = os_strchr(ptr, '\r');
                        if(ptr_1 != NULL){
                            uint8 size_str[10];
                            uint8 _i;
                            os_strncpy(&name[0], ptr, ptr_1-ptr);
                            name[ptr_1-ptr] = 0;
                            os_printf("File: %s\n", name);

                            if (file_system.number_of_files > 0){
                                file_t _file;
                                _i;
                                for (_i = 0; _i < file_system.number_of_files; _i++){
                                    spi_flash_read(FILES_ADDR+_i*sizeof(file_t), &_file, sizeof(file_t));
                                    if (os_strcmp(name, _file.name) == 0){
                                        uint8 *_read_file;
                                        uint32 _size = _file.size;
                                        uint8 _block = 0;
                                        os_printf("File founded. Size: %u\n", _file.size);
                                        if (_file.size > 4096){
                                            _read_file = (uint8 *)os_malloc(4096);
                                            while(_size > 4096){
                                                spi_flash_read(_file.address+4096*_block++, (uint32 *)_read_file, 4096);
                                                uart0_tx_buffer(_read_file, 4096);
                                                _size -= 4096;
                                            }
                                            spi_flash_read(_file.address+4096*_block++, (uint32 *)_read_file, (_size/4)*4 + (_size%4)*4);
                                            uart0_tx_buffer(_read_file, _size);
                                        }
                                        else{
                                            os_printf("Reading %u bytes\n", (_size/4)*4 + (_size%4)*4);
                                            _read_file = (uint8 *)os_malloc((_size/4)*4 + (_size%4)*4);
                                            spi_flash_read(_file.address, (uint32 *)_read_file, (_size/4)*4 + (_size%4)*4);
                                            uart0_tx_buffer(_read_file, _size);
                                        }
                                        os_free(_read_file);
                        //                 flash_bkp = (uint8 *)malloc(4096);
                        // spi_flash_read(FILES_ADDR, (uint32 *)flash_bkp, 4096);
                        // os_printf("------\n");
                        // for (int _i = 0; _i < 4096; _i++){
                        //     os_printf("%c", flash_bkp[_i]);
                        // }

                                        break;
                                    }
                                }
                                if (_i >= file_system.number_of_files)
                                    os_printf("File not founded\n");
                            }
                            else
                                os_printf("Without files\n");
                        }
                        else
                            os_printf("Comando inválido\n");
                    }
                    // else
                        // os_printf("Não entendi\n");
                }
                else{
                    command[pcommand++] = tmp_char;
                }
            }
        }
        else{
            flash_buffer[size_buffer++] = tmp_char;
            if (((size_buffer+4096*block_file) >= size) || (size_buffer >= 4096)){
                os_printf("Erasing sector %u (0x%08X): %d\n", (file_system.next_position+4096*block_file)>>12, (file_system.next_position+4096*block_file)>>12, spi_flash_erase_sector((file_system.next_position+4096*block_file)>>12));
                os_printf("Writng to address 0x%08X: %d\n", file_system.next_position+4096*block_file, spi_flash_write(file_system.next_position+4096*block_file, (uint32 *)flash_buffer, size_buffer));

                if ((size_buffer+4096*block_file) >= size){
                    free(flash_buffer);
                    
                    flash_bkp = (uint8 *)malloc(3840);
                    file_t *_file_ptr = flash_bkp;
                    spi_flash_read(FILES_ADDR, flash_bkp, 3840);
                    _file_ptr[file_system.number_of_files].address = file_system.next_position;
                    os_strncpy(_file_ptr[file_system.number_of_files].name, name, 24);
                    _file_ptr[file_system.number_of_files].size = size;
                    file_system.next_position += size_bytes;
                    file_system.number_of_files++;
                    file_system.free_size = file_system.free_size - size_bytes;
                    file_system.used_size = file_system.used_size + size_bytes;
                    os_printf("Erasing sector %u (0x%08X): %d\n", FILE_SYSTEM_ADDR/4096, FILE_SYSTEM_ADDR/4096, spi_flash_erase_sector(FILE_SYSTEM_ADDR/4096));
                    os_printf("Write to address 0x%08X: %d\n", FILE_SYSTEM_ADDR, spi_flash_write(FILE_SYSTEM_ADDR, (uint32 *)&file_system, sizeof(file_system)));
                    // os_printf("Erasing sector %u: %d\n", FILES_ADDR/4096, spi_flash_erase_sector(FILES_ADDR/4096));
                    os_printf("Write to address 0x%08X: %d\n", FILES_ADDR, spi_flash_write(FILES_ADDR, (uint32 *)flash_bkp, 3840));                
                    
                    free(flash_bkp);
                    
                    receiving_file = false;
                }
                size_buffer = 0;
                block_file++;
            }
        }
        RxBuffer.pOutPos++;
        RxBuffer.Space++;
        if (RxBuffer.pOutPos >= (RxBuffer.pUartBuff+RxBuffer.UartBuffSize))
            RxBuffer.pOutPos = RxBuffer.pUartBuff;
        // if (RxBuffer.Space == RxBuffer.UartBuffSize){
        uart_rx_intr_enable(UART0);
        // }
    }
}

void ICACHE_FLASH_ATTR
init_system(void)
{
    sint8 status;
    os_printf("inicialização terminada\r\n");

    wifi_station_disconnect();

    spi_flash_read(FILE_SYSTEM_ADDR, (uint32 *)&file_system, sizeof(file_system));

    if ((file_system.security_code_1 != 0xAA55AA55) || (file_system.security_code_2 != 0x55AA55AA)){
        os_printf("File system not initialized!\nInitializing file system");
        file_system.security_code_1 = 0xAA55AA55;
        file_system.security_code_2 = 0x55AA55AA;
        file_system.number_of_files = 0;
        file_system.total_size = 61440;
        file_system.free_size = 61440;
        file_system.used_size = 0;
        file_system.ptr = FILES_ADDR;
        file_system.next_position = FILES_BASE;

        spi_flash_erase_sector(FILE_SYSTEM_ADDR/4096);
        spi_flash_write(FILE_SYSTEM_ADDR, (uint32 *)&file_system, sizeof(file_system));
    }
    file_system.file = 0;


    os_printf("File system:\n");
    os_printf("Number of files: %u | ", file_system.number_of_files);
    os_printf("Total size: %u kB | ", file_system.total_size/1024);
    os_printf("Free size: %u kB | ", file_system.free_size/1024);
    os_printf("Used size: %u\n", file_system.used_size/1024);
 
    os_timer_setfn(&loop_timer, loop, NULL);
    os_timer_arm(&loop_timer, LOOP_TIMER_TIME, true);

}

void ICACHE_FLASH_ATTR 
user_init(void)
{
    struct rst_info *reset_info = system_get_rst_info();
    uart_init(BIT_RATE_115200, BIT_RATE_115200);
    set_uart_buffer(4096);
    wifi_set_opmode(STATION_MODE);
    system_update_cpu_freq(SYS_CPU_160MHZ);
    system_init_done_cb(init_system);
    os_printf("Reset info:\r\n->Reason: %u\r\n->Exception Cause: %u\r\n->Exception Address: 0x%08X\r\n->EPC1: 0x%08X\r\n->EPC2: 0x%08X\r\n->EPC3: 0x%08X\r\n->DEPC: 0x%08X\r\n", reset_info->reason, reset_info->exccause, reset_info->excvaddr, reset_info->epc1, reset_info->epc2, reset_info->epc3, reset_info->depc);
    os_printf("\n\n\nCPU frequency: %u Mhz", system_get_cpu_freq());
}


void ICACHE_FLASH_ATTR
user_pre_init(void) {
    if(!system_partition_table_regist(partition_table, sizeof(partition_table)/sizeof(partition_table[0]),SPI_FLASH_SIZE_MAP)) {
		os_printf("system_partition_table_regist fail\r\n");
		while(1);
	}
}