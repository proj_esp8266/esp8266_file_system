Desenvolvimento de uma biblioteca de um sistema de arquivos para ESP8266.
Criação: Rafael Ferro Luzia.

Status: Em desenvolvimento.
Data: 26/02/2022

Ambiente de compilação utilizado: VSCode + PlatformIO + NONOS-SDK 3.0.4

##########################################################################
26/02/2022 - Rafael Ferro Luzia
Desenvolvimento de uma biblioteca para um sistema de arquivos para ESP8266 que permite:
-> Salvamento de arquivo;
-> Remoção de arquivos (até o momento apenas é possível limpar todo o sistema de arquivos);
-> Visualização das informações dos arquivos (nome do arquivos, tamanho e localização na memória);
-> Leitura dos arquivos.

Ao final do desenvolvimento haverá uma biblioteca que permite a transferência de arquivos para o ESP8266 sem a necessidade de colocação do dispositivo em modo de gravação.
No momento a biblioteca está vinculada ao uso da UART para transferência dos arquivos, porém o objetivo é criar uma biblioteca não vinculada à interface de comunicação.
O sistema de arquivos é criado na Flash interna do dispositivo.

Comando permitidos:
OBS: todos os comando são em formato ASCII e devem ser finalizado com CR+LF
-> "ld": Ler as informações do sistema de arquivos (Quantidade de memória total, disponível e utilizada. Nome, tamanho e localização dos arquivos);
-> "erase": Apagar todos os arquivos;
-> "dw": Fazer o download de um arquivo para o dispositivo. 
    Formato: dw <nome do arquivo> <tamanho>
    O dispositivo irá responder ">" indicando que o arquivo pode ser transmitido. Os nomes de arquivos devem ser únicos. Ao executar este comando o dispositivo verificar os nomes existentes e caso o arquivo já exista retorna a mensagem "File already exist" e não permite a transferência do arquivos.
-> "up": Fazer o upload de um arquivo do dispositivo.
    Formato: up <nome do arquivo>
    O dispositivo irá responder com o tamanho do arquivo e enviar o arquivo. Caso o arquivo não exista retornará a mensagem "File not founded" indicando que o arquivo não foi encontrado.
    